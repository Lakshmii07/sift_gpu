#include "siftheadertest.h"
#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <unistd.h>
const int BLOCKSIZE = 16;


using namespace cv;

#define CudaSafeCall( err ) __cudaSafeCall( err, __FILE__, __LINE__ )
inline void __cudaSafeCall( cudaError err, const char *file, const int line )
{
#ifdef CUDA_ERROR_CHECK
if ( cudaSuccess != err )
{
fprintf( stderr, "cudaSafeCall() failed at %s:%i : %s\n",
file, line, cudaGetErrorString( err ) );
exit( -1 );
}
#endif
return;
}

__global__ void findkeypoints(float * down,float * middle,float * up,float * output, int rows, int cols)
{

int ty=threadIdx.y;
int tx=threadIdx.x;

int row = blockIdx.y * blockDim.y + ty;
int col = blockIdx.x * blockDim.x + tx;

double dxx, dyy, dxy, trH, detH;
double curvature_ratio;
double currentPixel;
bool justSet;

__shared__ float downblock[BLOCKSIZE+2][BLOCKSIZE+2];
__shared__ float upblock[BLOCKSIZE+2][BLOCKSIZE+2];
__shared__ float middleblock[BLOCKSIZE+2][BLOCKSIZE+2];

// Loading the shared memory

downblock[ty+1][tx+1]   = down[row*cols+col];
upblock[ty+1][tx+1]     = up[row*cols+col];
middleblock[ty+1][tx+1] = middle[row*cols+col];

if(tx == (BLOCKSIZE-1))
{
downblock[ty+1][tx+2]   = down[row*cols+(col+1)];
upblock[ty+1][tx+2]     = up[row*cols+(col+1)];
middleblock[ty+1][tx+2] = middle[row*cols+(col+1)];
}
if(ty == (BLOCKSIZE-1))
{
downblock[ty+2][tx+1]   = down[(row+1)*cols+(col)];
upblock[ty+2][tx+1]     = up[(row+1)*cols+(col)];
middleblock[ty+2][tx+1] = middle[(row+1)*cols+(col)];
}
if(tx == (BLOCKSIZE-1) && ty == (BLOCKSIZE-1))
{
downblock[ty+2][tx+2]   = down[(row+1)*cols+(col+1)];
upblock[ty+2][tx+2]     = up[(row+1)*cols+(col+1)];
middleblock[ty+2][tx+2] = middle[(row+1)*cols+(col+1)];
} 
if(tx == 0)
{
downblock[ty+1][tx]     = down[(row)*cols+(col-1)];
upblock[ty+1][tx]       = up[(row)*cols+(col-1)];
middleblock[ty+1][tx]   = middle[(row)*cols+(col-1)];
}
if(ty==0)
{
downblock[ty][tx+1]     = down[(row-1)*cols+(col)];
upblock[ty][tx+1]       = up[(row-1)*cols+(col)];
middleblock[ty][tx+1]   = middle[(row-1)*cols+(col)];
}
if(tx==0 && ty == 0)
{
downblock[ty][tx]       = down[(row-1)*cols+(col-1)];
upblock[ty][tx]         = up[(row-1)*cols+(col-1)];
middleblock[ty][tx]     = middle[(row-1)*cols+(col-1)];
}

 if ( row < rows && col < cols)
    {

     justSet = false;
     currentPixel = middleblock[ty+1][tx+1];

     		                                         
    		    if (currentPixel > middleblock[ty][tx+1] 	        &&
                        currentPixel > middleblock[ty+2][tx+1]          &&
                        currentPixel > middleblock[ty+1][tx]  	        &&
                        currentPixel > middleblock[ty+1][tx+2]          &&
                        currentPixel > middleblock[ty][tx]	        &&
                        currentPixel > middleblock[ty][tx+2]	        &&
                        currentPixel > middleblock[ty+2][tx+2]		&&
                        currentPixel > middleblock[ty+2][tx]		&&
                        currentPixel > upblock[ty+1][tx+1]          	&&
                        currentPixel > upblock[ty][tx+1]       	        &&
                        currentPixel > upblock[ty+2][tx+1]   		&&
                        currentPixel > upblock[ty+1][tx]                &&
                        currentPixel > upblock[ty+1][tx+2]              &&
                        currentPixel > upblock[ty][tx]	                &&
                        currentPixel > upblock[ty][tx+2]	        &&
                        currentPixel > upblock[ty+2][tx+2]	        &&
                        currentPixel > upblock[ty+2][tx]                &&
                        currentPixel > downblock[ty+1][tx+1]          	&&
                        currentPixel > downblock[ty][tx+1]       	&&
                        currentPixel > downblock[ty+2][tx+1]   		&&
                        currentPixel > downblock[ty+1][tx]              &&
                        currentPixel > downblock[ty+1][tx+2]            &&
                        currentPixel > downblock[ty][tx]	        &&
                        currentPixel > downblock[ty][tx+2]	        &&
                        currentPixel > downblock[ty+2][tx+2]	        &&
                        currentPixel > downblock[ty+2][tx])	
                        		{
					output[row * cols + col] = 255;
                                     	justSet = true;
					} 
               else if (currentPixel < middleblock[ty][tx+1] 	        &&
                        currentPixel < middleblock[ty+2][tx+1]          &&
                        currentPixel < middleblock[ty+1][tx]  	        &&
                        currentPixel < middleblock[ty+1][tx+2]          &&
                        currentPixel < middleblock[ty][tx]	        &&
                        currentPixel < middleblock[ty][tx+2]	        &&
                        currentPixel < middleblock[ty+2][tx+2]		&&
                        currentPixel < middleblock[ty+2][tx]		&&
                        currentPixel < upblock[ty+1][tx+1]          	&&
                        currentPixel < upblock[ty][tx+1]       	        &&
                        currentPixel < upblock[ty+2][tx+1]   		&&
                        currentPixel < upblock[ty+1][tx]                &&
                        currentPixel < upblock[ty+1][tx+2]              &&
                        currentPixel < upblock[ty][tx]	                &&
                        currentPixel < upblock[ty][tx+2]	        &&
                        currentPixel < upblock[ty+2][tx+2]	        &&
                        currentPixel < upblock[ty+2][tx]                &&
                        currentPixel < downblock[ty+1][tx+1]          	&&
                        currentPixel < downblock[ty][tx+1]       	&&
                        currentPixel < downblock[ty+2][tx+1]   		&&
                        currentPixel < downblock[ty+1][tx]              &&
                        currentPixel < downblock[ty+1][tx+2]            &&
                        currentPixel < downblock[ty][tx]	        &&
                        currentPixel < downblock[ty][tx+2]	        &&
                        currentPixel < downblock[ty+2][tx+2]	        &&
                        currentPixel < downblock[ty+2][tx])	
                        		{
					output[row * cols + col] = 255;
			                justSet = true;
					} 

                     if(justSet && fabs(currentPixel) < 0.03)
					{
					output[row * cols + col] = 0;
                                        justSet = false;	
					} 

					
                     if(justSet)
				{
				 dxx = (middleblock[ty][tx+1] +					  
                                        middleblock[ty+2][tx+1] -
		         	       2.0*currentPixel);

				 dyy = (middleblock[ty+1][tx] +
			               	middleblock[ty+1][tx+2] -
					2.0*currentPixel);

				 dxy = (middleblock[ty][tx] +
				        middleblock[ty+2][tx+2] -
					middle[(row+1) * cols + (col-1)] - 
					middle[(row-1) * cols + (col+1)]) / 4.0;

				 trH = dxx + dyy;
				 detH = dxx*dyy - dxy*dxy;

				 curvature_ratio = trH*trH/detH;

		    if(detH<0 || curvature_ratio > 7.2)
				{
				 output[row * cols + col] = 0;
                                 justSet=false;
                                }
				}
			
    }
}


__global__ void mag_ori_assign(float * gimage,float * output,float *output_2, int rows, int cols)
{
int row = blockIdx.y * blockDim.y + threadIdx.y;
int col = blockIdx.x * blockDim.x + threadIdx.x;
double dx, dy;
 if ( row < rows && col < cols)
    {
    dx = gimage[(row) * cols + (col+1)] - gimage[(row) * cols + (col-1)];
    dy = gimage[(row+1) * cols + (col)] - gimage[(row-1) * cols + (col)];  
    output[(row) * cols + (col)] = sqrt(dx*dx+dy*dy);
    output_2[(row) * cols + (col)] = atan(dy/dx);
    }
}



__global__ void Interpolation(float * output,float * output2,float * input,int rows, int cols,int width,int height)
{

int row = blockIdx.y * blockDim.y + threadIdx.y;
int col = blockIdx.x * blockDim.x + threadIdx.x;
double dx,dy;
register float dd;
unsigned int iii;
unsigned int jjj ;
int tx =threadIdx.x;
int ty = threadIdx.y;

__shared__ float inputblock[BLOCKSIZE+2][BLOCKSIZE+2];


if ( row>1.5 && row<(height-1.5) && col < (width-1.5) && width>1.5 )
{
inputblock[ty+1][tx+1]=input[row * col + col];

if( tx == 15)
{
inputblock[ty+1][tx+2]=input[(row) * cols + (col+1)];
}

if( ty == 15)
{
inputblock[ty+2][tx+1]=input[(row+1) * cols + (col)];
}

if( ty == 0)
{
inputblock[ty][tx+1]=input[(row-1) * cols + (col)];
}

if( tx == 0)
{
inputblock[ty+1][tx]=input[(row) * cols + (col-1)];
}

}
__syncthreads();
 
if ( row>1.5 && row<(height-1.5) && col < (width-1.5) && width>1.5 )
    {
   dd=inputblock[ty+1][tx+1];
   dx = ( inputblock[ty+1][tx+2] + dd)/2 - (inputblock[ty+1][tx] + dd)/2 ;
   dy = ( inputblock[ty+2][tx+1] + dd)/2 - (inputblock[ty][tx+1] + dd)/2 ;
   iii=col+1;
   jjj=row+1;
   output[(jjj) * cols + (iii)] = sqrt(dx*dx + dy*dy);
   output2[(jjj) * cols + (iii)] =(atan2(dy,dx)==M_PI)? -M_PI:atan2(dy,dx);
    }

}


IplImage ** keypoints(IplImage * middle,IplImage * up,IplImage * down,IplImage * down2)
{

 IplImage * m_extrema = cvCreateImage(cvGetSize(down),32, 1);
 cvZero(m_extrema);

 IplImage ** testimg = (IplImage**)malloc(sizeof(IplImage*) * 2);

  float *im = (float*)middle->imageData;
  float *im2 = (float *)up->imageData;  
  float  *im3 = (float *)down->imageData;
  float *im4 = (float *)down2->imageData;
  float *im_out=(float *)m_extrema->imageData; 

  float *devIm;
  float *devIm_2;
  float *devIm_3;
  float *devIm_4;

  float *devIm_output;
  float *devIm_output2;
 
  Mat B1 = cvarrToMat(middle);
  int csize = B1.rows * B1.cols * sizeof(float);



  
  CudaSafeCall( cudaMalloc( (void**)&devIm, csize ));
  CudaSafeCall( cudaMalloc( (void**)&devIm_2, csize ));
  CudaSafeCall( cudaMalloc( (void**)&devIm_3, csize ));
  CudaSafeCall( cudaMalloc( (void**)&devIm_4, csize ));
  CudaSafeCall( cudaHostAlloc( (void**)&devIm_output, csize,cudaHostAllocDefault ));
  CudaSafeCall( cudaMalloc( (void**)&devIm_output2, csize ));
  CudaSafeCall( cudaMemcpyAsync( devIm, im, csize, cudaMemcpyHostToDevice ));
  CudaSafeCall( cudaMemcpyAsync( devIm_2, im2, csize, cudaMemcpyHostToDevice ));
  CudaSafeCall( cudaMemcpyAsync( devIm_3, im3, csize, cudaMemcpyHostToDevice ));
  CudaSafeCall( cudaMemcpyAsync( devIm_4, im4, csize, cudaMemcpyHostToDevice ));

  CudaSafeCall( cudaMemcpyAsync( devIm_output2,im_out , csize, cudaMemcpyHostToDevice ));

  dim3 dimBlock( BLOCKSIZE, BLOCKSIZE );
  dim3 dimGrid( (B1.cols/BLOCKSIZE + 1), (B1.rows/BLOCKSIZE + 1 ));
  findkeypoints<<<dimGrid, dimBlock>>>(devIm,devIm_2,devIm_3,devIm_output,B1.rows , B1.cols);
  findkeypoints<<<dimGrid, dimBlock>>>(devIm_2,devIm_3,devIm_4,devIm_output2,B1.rows , B1.cols);
  cudaDeviceSynchronize();

  float *out = (float*)calloc(csize, sizeof(float));
  float *out2 = (float*)calloc(csize, sizeof(float));
 
  Mat outm,outm2;
 
  CudaSafeCall(  cudaMemcpy( out2, devIm_output2,csize, cudaMemcpyDeviceToHost ));
  
  CudaSafeCall( cudaFree( devIm ));
  CudaSafeCall( cudaFree( devIm_2 ));
  CudaSafeCall( cudaFree( devIm_3 ));
  CudaSafeCall( cudaFree( devIm_4 ));
  CudaSafeCall( cudaFree(devIm_output));
  CudaSafeCall( cudaFree(devIm_output2));
  
  outm.data=(uchar*)devIm_output;
  outm2.data=(uchar*)out2;
 

  IplImage copy = outm;
  testimg[0] = &copy;
  IplImage copy2 = outm2;
  testimg[1] = &copy2;

 

  return testimg ;

}



IplImage ** inbetween(IplImage * image,IplImage * image2,IplImage * gimage)
{

 IplImage ** testimg = (IplImage**)malloc(sizeof(IplImage*) * 2);
 int width=gimage->width;
 int height=gimage->height;
 

 Mat B1,B2;
 Mat B3 = cvarrToMat(gimage);


 float *im3 = (float*)B3.data;

 
 float *devIm;
 float *devIm2;
 float *gdev;

 int csize = B3.rows * B3.cols * sizeof(float);

 CudaSafeCall( cudaHostAlloc( (void**)&devIm,csize,cudaHostAllocDefault));
 CudaSafeCall( cudaHostAlloc( (void**)&devIm2, csize,cudaHostAllocDefault));
 CudaSafeCall( cudaMalloc( (void**)&gdev, csize ));


  CudaSafeCall( cudaMemcpy( gdev, im3, csize , cudaMemcpyHostToDevice ));

  dim3 dimBlock( BLOCKSIZE, BLOCKSIZE );
  dim3 dimGrid( (B3.cols/BLOCKSIZE + 1), (B3.rows/BLOCKSIZE + 1 ));

  Interpolation<<<dimGrid, dimBlock>>>(devIm,devIm2,gdev,B3.rows,B3.cols,width,height);
 
  float *out = (float*)calloc(csize, sizeof(float));
  float *out2 = (float*)calloc(csize,sizeof(float));

  cudaDeviceSynchronize();
 
  CudaSafeCall(  cudaMemcpy( out, devIm,csize, cudaMemcpyDeviceToHost ));
  CudaSafeCall(  cudaMemcpy( out2, devIm2,csize, cudaMemcpyDeviceToHost ));


  B1.data=(uchar *)out;
  B2.data=(uchar *)out2;

 
  IplImage copy = B1;
  testimg[0] = &copy;

  IplImage copy2 = B2;
  testimg[1] = &copy2;

   
  return testimg;
}

IplImage ** orientation_cuda(IplImage * gimage)
{

 IplImage** testimg= (IplImage**)malloc(sizeof(IplImage*) * 2);

 testimg[0] = cvCreateImage(cvGetSize(gimage),32, 1);
 testimg[1] = cvCreateImage(cvGetSize(gimage),32, 1);
 
  
  Mat B1 = cvarrToMat(gimage);
  float *im = (float*)B1.data;
 
  Mat outm;
  Mat outo;
   
  float *devIm;
  float *devIm_output;
  float *devIm_output2;

  int csize = B1.rows * B1.cols * sizeof(float);

  CudaSafeCall( cudaMalloc( (void**)&devIm, csize ));
  CudaSafeCall( cudaMallocHost( (void**)&devIm_output, csize ));
  CudaSafeCall( cudaMallocHost( (void**)&devIm_output2, csize ));
  CudaSafeCall( cudaMemcpy( devIm, im, csize, cudaMemcpyHostToDevice ));


  dim3 dimBlock( BLOCKSIZE,BLOCKSIZE );
  dim3 dimGrid( (B1.cols/BLOCKSIZE + 1), (B1.rows/BLOCKSIZE + 1 ));
  mag_ori_assign<<<dimGrid, dimBlock>>>(devIm,devIm_output,devIm_output2,B1.rows , B1.cols);


  cudaDeviceSynchronize();

  CudaSafeCall( cudaFree( devIm ));
  CudaSafeCall( cudaFree(devIm_output));
  CudaSafeCall( cudaFree(devIm_output2));

  outm.data = (uchar*)devIm_output;
  outo.data = (uchar*)devIm_output2;

  IplImage copy = outm;
  testimg[0] = &copy;

  IplImage copy2 = outo;
  testimg[1] = &copy2;

  return testimg ;

}




