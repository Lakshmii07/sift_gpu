#ifndef SIFTHEADERTEST_H
#define SIFTHEADERTEST_H
#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "siftheadertest.h"
using namespace cv;
IplImage ** keypoints(IplImage * middle,IplImage * up,IplImage * down,IplImage * down2); 
IplImage ** orientation_cuda(IplImage * gimage);
IplImage ** inbetween(IplImage * image,IplImage * image2,IplImage * gimage);
#endif
